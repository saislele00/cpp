/*Nome:Raffaele
  Cognome:Sais
  Classe: 3Ei
*/
#include <iostream>
using namespace std;
int main(){
	int Assenze[500],MVoti[500],i,n,contA,scelta,m,d,Am,cont,cont2;
	float MediaSuff,MediaIns,MediaA;
	string Alunni[500];
//Primo Alunno Esempio
Alunni[0]="Sais";
MVoti[0]=8.2;
Assenze[0]=21;
//Secondo Alunno Esempio
Alunni[1]="Paiotta";
MVoti[1]=4.5;
Assenze[1]=14;
n=2;
cont=0;
cont2=0;
//Scelta Switch
cout<<"               BENVENUTO!"<<endl;
menu:
cout<<"Decidere Quale Operazione Effettuare: "<<endl;
cout<<"1.             Inserimento Alunno/Medie/Assenze"<<endl;
cout<<"2.             Ricerca Assenze Alunno"<<endl;
cout<<"3.             Ricerca Alunni Con Media Compresa Tra 7 E 9"<<endl;
cout<<"4.             Ricerca Percentuale Alunni Con Media Sufficiente E Non"<<endl;
cout<<"5.             Ricerca Percentuale Assenze Alunno"<<endl;
cin>>scelta;
switch(scelta){
	//Inserimento Alunni
	case (1):{
		cout<<"Quanti Alunni Inserire?: ";
		cin>>m;
		d=m+n;
		for(i=n;i<d;i++){
			cout<<"Inserisci Nome Alunno: ";
			cin>>Alunni[i];
			cout<<"Inserisci La Media Voti Di "<<Alunni[i]<<" : ";
			cin>>MVoti[i];
			cout<<"Inserisci Il Numero Di Assenze Di "<<Alunni[i]<<" : ";
			cin>>Assenze[i];
			n++;
		}
		break;
	}
	//Ricerca Informazioni Alunni
	case(2):{
		cout<<"Inserire Il Numero Minimo Di Assenze: ";
		cin>>Am;
		for(i=0;i<n;i++){
			if(Assenze[i]>Am){
				cout<<Alunni[i]<<" : "<<Assenze[i]<<endl;
				cont++;
			}
		}
		if(cont>0){
			cout<<"Ci Sono "<<cont<<" Persone Che Rientrano Nell' Intervallo Da Lei Scritto."<<endl;
		}
		else{
			cout<<"Nessun Alunno Rientra Nell' Intervallo Da Lei Scritto."<<endl;
		}
		cout<<endl;
		break;
	        }
		//Media Tra 7 E 9
		case(3):{
		cont=0;
		for(i=0;i<n;i++){
			if(MVoti[i]>6 && MVoti[i]<10){
				cout<<Alunni[i]<<" : "<<MVoti[i]<<endl,
				cont++;
			}
		}
		if(cont>0){
			cout<<"Sono Presenti "<<cont<<" Alunni Con La Media Tra Il 7 E Il 9"<<endl;
		}
		else{
			cout<<"Non Ci Sono Alunni Che Rientrano In Questi Intervalli"<<endl;
		}
		cout<<endl;
		break;
		}
		//Determinare Percentuale Alunni Sufficienti E Non
		case(4):{
		cont=0;
		for(i=0;i<n;i++){
			if(MVoti[i]>=6){
				cont++;
			}
			else{
				cont2++;
			}
		}
		MediaSuff=(100*cont)/n;
		MediaIns=(100*cont2)/n;
		cout<<"La Percentuale Di Alunni Con Un Voto Sufficiente E': "<<MediaSuff<<" %"<<endl;
		cout<<"La Percentuale Di Alunni Con Un Voto Insufficiente E': "<<MediaIns<<" %"<<endl;
		cout<<endl;
		break;
		}
		//Percentuali Assenza
		case(5):{
			
		contA=0;
		for(i=0;i<n;i++){
			contA=contA+Assenze[i];
		}
		MediaA=(200*contA)/100;
		cout<<"La Percentuale Di Assenze Nella Classe E' Del: "<<MediaA<<" %"<<endl;
		cout<<"Hanno Superato La Media Delle Assenze Di Classe Questi Alunni:"<<endl;
		for(i=0;i<n;i++){
			if(MediaA>Assenze[i]){
				cout<<Alunni[i]<<" Con "<<Assenze[i]<<endl;
			}
		}
		break;
	}
	default:{
		cout<<"Inserire Un Operazione Corretta!"<<endl;
		break;
	}
}
cout<<endl;
 cout<<"Eseguire Un'Altra Operazione? "<<endl;
 cout<<"1. Si"<<endl;
 cout<<"2. No"<<endl;
 cin>>scelta;
 if(scelta==1){
 	goto menu;
 }
 return 0;
}
