/* Nome: Gabriele
   Cognome: Pocchia
   Classe: 3E
   Specializzazione: Informatica
   Data di Creazione: 07/11/2016
   nome file: A163EP001.C
   Revisione: 0
   Data di Revisione: N/D
   Progetto: Dati n numeri calcolare la Media
*/
#include <iostream>
#include <math.h>
using namespace std;
int main(){
	int n_val,n_cont,num,media,n_contmedia;
	n_contmedia=0;
	n_val=0;
	cout<<"Quanti Numeri Desidera Inserire?";
	cin>>n_cont;
	while(n_cont!=0){
		cout<<"Inserire Un Numero:";
		cout<<endl;
		cin>>num;
		n_val=n_val+num;
		n_cont=n_cont-1;
		n_contmedia=n_contmedia+1;
	}
media=n_val/n_contmedia;
cout<<"La Media Dei Numeri e': "<<media<<" "<<endl;
return 0;
}
