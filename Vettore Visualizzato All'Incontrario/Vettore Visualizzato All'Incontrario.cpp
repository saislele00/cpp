/* Nome: Gabriele
   Cognome: Pocchia
   Classe: 3E
   Specializzazione: Informatica
   Data di Creazione: 05/11/2016
   nome file: A163EP021.C
   Revisione: 0
   Data di Revisione: N/D
   Progetto:Visualizzare Variabili All'Incontrario
*/
#include <iostream>
using namespace std;
int main(){
   int numeri[100],i,N;
   	cout<<"Inserisci Quanti Numeri Inserire: ";
   	cin>>N;
   	for(i=0;i<N;i++){
   		cout<<"Inserisci Il "<<i+1<<" Numero: ";
   		cin>>numeri[i];
	   }
	for(i=N-1;i>=0;i--){
		cout<<numeri[i];
	}
	return 0;
}
