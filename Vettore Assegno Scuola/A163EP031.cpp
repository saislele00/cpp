#include<iostream>
using namespace std;

//Funzione Per Verificare Se X E' Presente Nel Vettore
void Verifica(int v[],int x,int n){
	int cont=0;
	for(int i=0;i<n;i++){
		if(v[i]==x)
		cont++;
	}
	if(cont!=0)
			cout<<"L'Elemento E' Presente "<<cont<<" Volta/e"<<endl;
			else
			cout<<"L'Elemento Non E' Presente Nel Vettore"<<endl;
}

//Funzione Per Ordinare Un Vettore
int Ordinamento(int v[],int n){
	int i,min,temp,j;
	for(i=0; i<n-1; i++){ 
min = i;
for(j=i+1; j<n; j++) 
  if(v[j] < v[min])
    min = j;
temp=v[min]; 
v[min]=v[i]; 
v[i]=temp;}
}

//Funzione Che Elimina I Duplicati
int Duplicati(int v[],int n){
	int v2[100];
	int i;
	int j=0;
	int m=0;
	for(i=0;i<n;i++){
		if(v[i]!=v[i+1]){
			v2[j]=v[i];
			j++;
			m++;
		}
	}
	for(j=0;j<m;j++){
		cout<<v2[j]<<" ";
	}
}


//Funzione Per Calcolare La Media
int Media(int v[],int n){
	int Media;
	int i;
	int cont=0;
	for(i=0;i<n;i++){
		cont=cont+v[i];
	}
	Media=cont/n;
	return Media;
}

int main(){
	int v[100],i,n,k,cont,x,scelta,m,v2,j;
	cout<<"Inserire La Dimensione Del Vettore: ";
	cin>>n;
	for(i=0;i<n;i++){
		cout<<"Inserisci Il "<<i+1<<" Numero: "<<endl;
		cin>>v[i];
		}
	do{
	cout<<"         MENU'       "<<endl;
	cout<<endl;
	cout<<"1.      Verificare Se X E' Presente Nel Vettore"<<endl;
	cout<<"2.      Eliminare Le Ricorrenze"<<endl;
	cout<<"3.      Fare La Media Del Vettore"<<endl;
	cout<<"4.      Visualizzare Il Vettore Ordinato"<<endl;
	cout<<"5.      Pulire Lo Schermo"<<endl;
	cout<<"0.      Uscire Dal Programma"<<endl;
	cin>>scelta;
	switch(scelta){
	    //Verifica Di Un Numero All'Interno Del Vettore
		case 1:{
			cout<<"Inserisci Il Numero Da Verificare: ";
			cin>>x;
			Verifica(v,x,n);
			break;
		}
		//Eliminare Le Ricorrenze
		case 2:{
			Ordinamento(v,n);
			Duplicati(v,n);
			cout<<endl;
			break;
		}
		//Media Degli Elementi
		case 3:{
			cout<<"La Media Degli Elementi Presenti Nel Vettore E': "<<Media(v,n)<<endl;
			break;
		}
		//Ordinamento Di Un Vettore
		case 4:{
			Ordinamento(v,n);
			cout<<"Verra Ora Visualizzato Il Vettore In Modo Ordinato"<<endl;
			for(i=0;i<n;i++){
				cout<<v[i]<<"  ";
			}
			cout<<endl;
			break;
		}
		//Pulizia Schermo!1!1
		case 5:{
			system("CLS");
			break;
		}
		default:{
			system("CLS");
			cout<<"Operazione Non Possibile...Riprovare"<<endl;
			system("PAUSE");
			break;
		}
	}
    }while(n!=0);
	return 0;
}
