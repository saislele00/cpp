/* Nome: Gabriele
   Cognome: Pocchia
   Classe: 3E
   Specializzazione: Informatica
   Data di Creazione: 05/11/2016
   nome file: A163EP001.C
   Revisione: 0
   Data di Revisione: N/D
   Progetto: Convertitore Lire-Euro O Euro-Lire
*/
#include <iostream>
#include <math.h>
using namespace std;
int main(){
	float lire,euro;
	int scelta;
	cout<<"Quale Operazione Si Vuole Effettuare?"<<endl;
	cout<<"1. Euro---->Lire"<<endl;
	cout<<"2. Lire---->Euro"<<endl;
	cin>>scelta;
	switch(scelta){
		case(1):{
			cout<<"Inserire Il Valore In Euro: ";
			cin>>euro;
			lire=1937.27 * euro;
			cout<<"Conversione Effettuata! Sono: "<<lire<<" Lire."<<endl;}
		case(2):{
			cout<<"Inserire Il Valore In Lire: ";
			cin>>lire;
			euro=lire / 1937.27;
			cout<<"Conversione Effettuata! Sono: "<<euro<<" Euro."<<endl;}
	}
system("PAUSE");
}
