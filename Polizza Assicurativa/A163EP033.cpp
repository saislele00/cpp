#include <iostream>
#include <stdlib.h>
using namespace std;
//Inserimento Polizza
void Ins_Polizza(int Codice[],int Prezzo[],int Quant[],int n){
	cout<<"Quante Polizze Inserire: "<<endl;
	cin>>n;
	for(int i=0;i<n;i++){
		cout<<"Inserisci Il Codice Della "<<i+1<<" Polizza: ";
		cin>>Codice[i];
		cout<<"Inserisci Il Prezzo Della Polizza: ";
		cin>>Prezzo[i];
		cout<<"Inserire La Quantita' Dei Versamenti Effettuati Per La Polizza: ";
		cin>>Quant[i];
	}
}
	
//Trovare Polizze Maggiori Avendo Un Input
void Pol_Max(int Codice[],int Prezzo[],int Quant[],int n){
	int x;
	int cont=0;
	cout<<"Inserire Il Prezzo Minimo: ";
	cin>>x;
	for(int i=0;i<n;i++){
		if(Prezzo[i]>=x){
			cout<<"Codice Polizza: "<<Codice[i]<<" Con Prezzo: "<<Prezzo[i]<<endl;
			cont++;
		}
	}
	cout<<"Ci Sono "<<cont<<" Polizze Che Superano Il Prezzo Dato In Inuput"<<endl;
}

//Numeri Versamenti Polizza
void Controllo_Vers(int Codice[],int Prezzo[],int Quant[],int n){
	int x;
	bool flag=false;
	int i=0;
	cout<<"Inserire Codice Della Polizza Da Controllare: ";
	cin>>x;
	while(flag==0){
		flag=false;
		if(Codice[i]==x){
			flag=true;
			cout<<"Nella Polizza Con Codice: "<<Codice[i]<<" Sono Stati Effettuati "<<Quant[i]<<" Versamenti"<<endl;
		}
		i++;
	             }
}

//
Prezzo_Vers(int Codice[],int Prezzo[],int Quant[],int n){
	int Prezzo_Totale;
	int x;
	bool flag=false;
	int i=0;
	cout<<"Inserire Codice Della Polizza Da Controllare: ";
	cin>>x;
	while(flag==0){
		flag=false;
		if(Codice[i]==x){
			flag=true;
			cout<<"Nella Polizza Con Codice: "<<Codice[i]<<" Sono Stati Effettuati "<<Quant[i]<<" Versamenti"<<endl;
			Prezzo_Totale=Quant[i]*Prezzo[i];
			cout<<"Con Un Prezzo Totale Di: "<<Prezzo_Totale<<endl;
		}
		i++;
	             }
}
int main(){
	int Codice[500],Prezzo[500],Quant[500],i,scelta,x;
	int n=0;
	//Menu
	do{
	cout<<"\tMenu"<<endl;
	cout<<"1.     Inserire Polizza"<<endl;
	cout<<"2.     Trovare Polizze Maggiori Avendo Un Input"<<endl;
	cout<<"3.     Verificare Quanti Versamenti Sono Stati Effettuati Per Una Polizza"<<endl;
	cout<<"4.     Versamenti Effettuati Di Una Polizza"<<endl;
	cout<<"5.     Pulire Schermo"<<endl;
	cout<<"0.     Uscire Dal Programma"<<endl;
	cin>>scelta;
	switch(scelta){
		//Case 1=Inserimento Polizza
		case 1:{
			Ins_Polizza(Codice,Prezzo,Quant,n);
			break;
		}
		//Case 2=Trovare Polizze Maggiori
		case 2:{
			Pol_Max(Codice,Prezzo,Quant,n);
			break;
		}
		//Case 3=Controllo Versamenti
		case 3:{
			Controllo_Vers(Codice,Prezzo,Quant,n);
			break;
		}
		//Case 4=Versamenti Effettuati E Prezzo
		case 4:{
			Prezzo_Vers(Codice,Prezzo,Quant,n);
			break;
		}
		//Case 5=Pulire Lo Schermo
		case 5:{
			system("CLS");
			break;
		}
	}
     }while(scelta!=0);
	return 0;
}
