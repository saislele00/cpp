
//Librerie
#include <iostream>
#include <math.h>
using namespace std;
main(){
	float a,b,c; //Valori
	float x_1,x_2; //Soluzioni
	float delta;
	cout<<"Questo Programma Permette Di Risolvere Un'Equazione Di Secondo Grado Del Tipo:";
	cout<<"ax^2+bx+c=0"<<endl;
	cout<<"Inserisci Il Valore Di a:";
	cin>>a;
	
	cout<<"Inserisci Il Valore Di b:";
	cin>>b;
	cout<<"Inserisci Il Valore Di c:";
	cin>>c;
	delta=(b*b) -4* a*c ;
	if(a==0)
	   if(b!=0){
		cout<<"In questo caso verra' risolta un'equazione di primo grado"<<endl;
		cout<<"Verra' Ora Calcolata La Soluzione"<<endl;
		x_1=- (c/b);
		cout<<"La Soluzione e':"<<x_1<<endl;
	           }     
	else
	           if(c!=0)
	                cout<<"Soluzione Impossibile"<<endl;
	            else
	                cout<<"Soluzione Indeterminata"<<endl;
	if(a!=0){
	if(delta>0){
		cout<<"Verranno Ora Calcolate Le 2 Possibili Soluzioni, ";
		cout<<"In Quanto Ci Sono 2 Soluzioni Reali e Distinte"<<endl;
	system("PAUSE");
	x_1= (-b+sqrt(delta))/2*a;
	x_2= (-b-sqrt(delta))/2*a;
	cout<<"La Prima Soluzione e': "<<x_1<<endl;
	cout<<"La Seconda Soluzione e': "<<x_2<<endl;
	cout<<"Soluzione Trovate Correttamente...Il Programma Verra' Arrestato!"<<endl;
	}
	if(delta<0){
		cout<<"Equazione Impossibile Da Risolvere Avendo Soluzioni Complesse Coniugate"<<endl;
	}
	if(delta==0){
		cout<<"Verra' Calcolata Un' Unica Soluzione ";
		cout<<"In Quanto Sono Tra Loro 2 Soluzioni Reali e Concidenti"<<endl;
	system("PAUSE");
	x_1=-b/(2*a);
	cout<<"La Soluzione e': "<<x_1<<endl;
	cout<<"Soluzione Trovata Correttamente...Il Programma Verra' Arrestato!"<<endl;
	}
}
system("PAUSE");	
}

