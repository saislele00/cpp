/*Compito In Classe
  Svolto Il: 29/11/16
  Prof: Picerno Lucia
  Alunno:Raffaele Sais
  Classe:3Ei
*/
#include <iostream>
using namespace std;
int main(){
	int vettvotis[1000],vettvotio[1000],i,n,votomax,cont;
	string vettnomi[1000],nome;
	cont=0;
	cout<<"Quanti Sono I Partecipanti?(1-1000) ";
	cin>>n;
	for(i=0;i<n;i++){
		cout<<"Inserisci Il Nome Del "<<i+1<<" Concorrente: ";
		cin>>vettnomi[i];
	}
	for(i=0;i<n;i++){
		cout<<"Inserire Il Voto Orale Di "<<vettnomi[i]<<": ";
		cin>>vettvotio[i];
		cout<<"Inserire Il Voto Scritto Di "<<vettnomi[i]<<": ";
		cin>>vettvotis[i];
	}
	votomax=0;
	for(i=0;i<n;i++){
		if(vettvotio[i]>votomax){
			votomax=vettvotio[i];
			nome=vettnomi[i];}
	}
	cout<<nome<<" Ha Ottenuto Il Voto Orale Maggiore, Ovvero: "<<votomax<<endl;
	cout<<"Verranno Ora Visualizzati Tutti I Partecipaneti Con I Rispettivi Voti:"<<endl;
	system("PAUSE");
	for(i=0;i<n;i++){
		cout<<vettnomi[i]<<" Ha Ottenuto "<<vettvotis[i]<<" Come Voto Scritto E "<<vettvotio[i]<<" Come Voto Orale."<<endl<<endl<<endl;
	}
	cout<<"Verranno Ora Visualizzati Tutti I Partecipanti Che Hanno Ottenuto Lo Stesso Voto Orale E Scritto: "<<endl;
	system("PAUSE");
	for(i=0;i<n;i++){
		if(vettvotio[i]==vettvotis[i]){
			cout<<vettnomi[i]<< "Ha Ottenuto Lo Stesso Punteggio Sia Nella Verifica Orale Che Scritta, Ovvero: "<<vettvotio[i];
		    cont++;
		}
	}
	if(cont==0){
			cout<<"Nessun Partecipante Ha Ottenuto Lo Stesso Punteggio Alle Due Prove";
		}
	return 0;
}
